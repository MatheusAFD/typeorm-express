import { Request, Response, Router } from 'express';
import { getCustomRepository, getRepository, Repository } from 'typeorm';
import { Class } from '../models/Class';
import { ClassRepository } from '../repositories/ClassRepository';

const classRouter = Router();

classRouter.post('/', async (request: any, response: any) => {
  try {
    const repo = getRepository(Class);
    const res = await repo.save(request.body);
    return response.status(201).json(res);
  } catch (err) {
    console.log('err.message :>>', err);
  }
});

classRouter.get('/', async (request: any, response: any) => {
  response.json(await getRepository(Class).find());
});

classRouter.get('/:name', async (request: any, response: any) => {
  const repository = getCustomRepository(ClassRepository);
  const res = await repository.findByName(request.params.name);
  return response.json(res);
});

export default classRouter;
